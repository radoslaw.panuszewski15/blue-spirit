package pl.semidude.bluespirit

import android.Manifest
import android.app.AlertDialog
import android.bluetooth.*
import android.bluetooth.le.BluetoothLeScanner
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.AsyncTask
import android.os.Bundle
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var btManager: BluetoothManager
    private var btAdapter: BluetoothAdapter? = null
    private lateinit var btScanner: BluetoothLeScanner
    private lateinit var btGatt: BluetoothGatt
    private var btCharacteristic: BluetoothGattCharacteristic? = null

    /**
     * Callback for results of device scanning
     */
    private val scanCallback = object : ScanCallback() {

        override fun onScanResult(callbackType: Int, result: ScanResult) {
            if (result.device.name == "BlueSpiritBLE") {
                stopScanning()
                btGatt = result.device.connectGatt(this@MainActivity, false, bluetoothGattCallback)
            }
        }
    }

    /**
     * Callback for connecting and discovering process
     */
    private val bluetoothGattCallback = object : BluetoothGattCallback() {

        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
            if (newState == BluetoothGatt.STATE_CONNECTED) {
                btGatt.discoverServices()
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                obtainCharacteristic()
            }
        }
    }

    /**
     * Finds LED service, and within that service looks for LED brightness characteristic
     */
    private fun obtainCharacteristic() {
        val btService: BluetoothGattService? =
            btGatt.getService(UUID.fromString("19B10000-E8F2-537E-4F6C-D104768A1214"))

        if (btService == null) {
            showToast("Service not found")
            return
        }

        btCharacteristic = btService.getCharacteristic(UUID.fromString("19B10001-E8F2-537E-4F6C-D104768A1214"))

        if (btCharacteristic == null) {
            showToast("Characteristic not found")
            return
        }

        showToast("Connection with LED service has been established")
        runOnUiThread { brightnessBar.isEnabled = true }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ensureBluetoothAvailable()
        ensureLocationAccess()
        setupBrightnessBar()
        startScanning()
    }

    /**
     * Obtain bluetooth handlers and if BT not enabled then prompt user to enable it
     */
    private fun ensureBluetoothAvailable() {
        btManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        btAdapter = btManager.adapter
        btScanner = btAdapter!!.bluetoothLeScanner


        if (btAdapter != null && !btAdapter!!.isEnabled) {
            val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT)
        }
    }

    /**
     * Check against location coarse permission, if not provided then prompt user
     */
    private fun ensureLocationAccess() {
        if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("This app needs location access")
            builder.setMessage("Please grant location access so this app can detect peripherals.")
            builder.setPositiveButton(android.R.string.ok, null)
            builder.setOnDismissListener {
                requestPermissions(
                    arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                    PERMISSION_REQUEST_COARSE_LOCATION
                )
            }
            builder.show()
        }
    }

    /**
     * Basic setup of SeekBar used for controlling brightness
     */
    private fun setupBrightnessBar() {
        brightnessBar.isEnabled = false

        brightnessBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                val multiplier = progress / 100.0
                val brightness = multiplier * 255
                writeCharacteristic(brightness.toInt())
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) { }

            override fun onStopTrackingTouch(seekBar: SeekBar?) { }
        })
    }

    /**
     * Write value for LED brightness characteristic
     */
    private fun writeCharacteristic(value: Int) {
        val array = ByteArray(1)
        array[0] = (value and 0xFF).toByte()
        btCharacteristic?.let { characteristic ->
            characteristic.value = array
            btGatt.writeCharacteristic(characteristic)
        }
    }

    private fun startScanning() {
        AsyncTask.execute { btScanner.startScan(scanCallback) }
    }

    private fun stopScanning() {
        AsyncTask.execute { btScanner.stopScan(scanCallback) }
    }

    private fun showToast(message: String) {
        runOnUiThread {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_REQUEST_COARSE_LOCATION -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    println("coarse location permission granted")
                } else {
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle("Functionality limited")
                    builder.setMessage("Since location access has not been granted, this app will not be able to discover beacons when in the background.")
                    builder.setPositiveButton(android.R.string.ok, null)
                    builder.setOnDismissListener { }
                    builder.show()
                }
                return
            }
        }
    }

    companion object {
        private const val REQUEST_ENABLE_BT = 1
        private const val PERMISSION_REQUEST_COARSE_LOCATION = 1
    }
}