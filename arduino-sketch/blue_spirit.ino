#include <CurieBLE.h>

BLEPeripheral blePeripheral;  
BLEService ledService("19B10000-E8F2-537E-4F6C-D104768A1214");

BLEUnsignedCharCharacteristic switchCharacteristic("19B10001-E8F2-537E-4F6C-D104768A1214", BLERead | BLEWrite);

const int ledPin = 9;

void setup() 
{
  Serial.begin(9600);

  pinMode(ledPin, OUTPUT);

  blePeripheral.setLocalName("BlueSpiritBLE");
  blePeripheral.setAdvertisedServiceUuid(ledService.uuid());

  blePeripheral.addAttribute(ledService);
  blePeripheral.addAttribute(switchCharacteristic);

  switchCharacteristic.setValue(0);
  blePeripheral.begin();
}

void loop() 
{
  BLECentral central = blePeripheral.central();

  if (central) {
    while (central.connected()) {
      if (switchCharacteristic.written()) 
          analogWrite(ledPin, switchCharacteristic.value());         
    }
  }
}
